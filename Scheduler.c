/***
***Tecnologico de estudios superiores de jocotitlan
***Ingenieria en sistemas Computacionales
***Nombre: Gustavo Perez Barrios
***Proyecto: Scheduler
***IC-602
***Ultima modificacion: 30-03-2020 15:20p.m.
***
***/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define _MAX_TAREAS 10 // 10 Numero de tareas maximas
#define _MAX_DESCRIPCION 101 //  maximo de caracters de la descripcion 100

typedef struct {

    int hr; // Almacenara las horas
    int minuto; // Almacenara los minutos
    char descripcion[_MAX_DESCRIPCION]; //Almacenara la descripcion de la tarea

} evento;

// Imprime el menu de selccion
void ImprimeMenu() {

    puts("+------ SCHEDULER -------+\n"
        "|  1. Nueva Tarea         |\n"
        "|  2. Eliminar Tarea      |\n"
        "|  3. Mostrar Tareas      |\n"
        "|  4. Ejecutar Tareas     |\n"
        "|  5. Salir               |\n"
        "+------------------------+\n");

}

// Retorna verdadero si el nulo
bool nulo(const evento *e) { return e == NULL; }

// Asigna memoria dinamica mediante malloc e
// inicializa una tarea en 0 y vacio
evento *inicializarTarea() {
    evento *e = (evento*)malloc(sizeof(evento));

    e->hr = 0;
    e->minuto = 0;
    strcpy(e->descripcion, "");

    return e;
}

// Tome la entrada del usuario hasta que el valor esté entre min y max inclusive, devuelva la entrada
int digitaRango(const int min, const int max) {

    int entrada = 0;
    char temp[21];
    char *mensaje = "| Digita un numero entre %d y %d: ";

    printf(mensaje, min, max);

    fgets(temp, 21, stdin);

    //atoi convierte cadena en valor numerico
    entrada = atoi(temp);

    //validacion de entradas
    while (entrada > max || entrada < min) { // Data validation
        printf(mensaje, min, max);
        fgets(temp, 21, stdin);
        entrada = atoi(temp);
    }
     /*do{
        printf(mensaje,min,max);
        fgets(temp,21,stdin);
        entrada=atoi(temp);
    }while(entrada>max||entrada<min);
    */

    return entrada;

}

//Crea una nueva taea con la entrada del usuario y devuelve un puntero a la misma tarea
evento* nuevaTarea(evento *e) {
    //si e es nulo lo inicializa
    if (nulo(e)) {
        e = inicializarTarea();
    }

    char *seperador = "+--------------------------------+";

    printf("\n%s\n|           NUEVA TAREA            |\n%s\n\n", seperador, seperador);

    puts("+---------- TIEMPO DE LA TAREA ----------+");

    e->hr = digitaRango(0, 23);
    e->minuto = digitaRango(0, 59);

    puts(seperador);

    puts("\n+--- DESCRIPCION DEL EVENTO ---+");

    printf("%s", "| DIGITA LA DESCRIPCION ");

    fgets(e->descripcion, _MAX_DESCRIPCION, stdin);

    puts("+-------------------------+\n");

    puts("| TAREA AGREGADA EXITOSAMENTE\n");

    return e;
}

// Agrega eventos a la list
void agregarTareaIndice(evento list[], const evento e, const int i) {

    if (nulo(&e)) {
        return;
    }

    list[i].hr = e.hr;
    list[i].minuto = e.minuto;
    strcpy(list[i].descripcion, e.descripcion);

}
// Ordenamiento por inserción
void ordenar(evento list[], const int tam) {

    for (int i = 1; i < tam; i++) {
        for (int j = i; j > 0 && (list[j - 1].hr > list[j].hr || (list[j - 1].hr == list[j].hr && list[j - 1].minuto > list[j].minuto)); j--) {
            int hrJ = list[j].hr;
            int minutoJ = list[j].minuto;
            char descripcionJ[_MAX_DESCRIPCION];
            strcpy(descripcionJ, list[j].descripcion);

            int hrJMenos1 = list[j - 1].hr;
            int minutoJMenos1 = list[j - 1].minuto;
            char descripcionJMenos1[_MAX_DESCRIPCION];
            strcpy(descripcionJMenos1, list[j - 1].descripcion);

            list[j].hr = hrJMenos1;
            list[j].minuto = minutoJMenos1;
            strcpy(list[j].descripcion, descripcionJMenos1);

            list[j - 1].hr = hrJ;
            list[j - 1].minuto = minutoJ;
            strcpy(list[j - 1].descripcion, descripcionJ);
        }
    }
}

//Inserta y ordena
void ordenaInserta(evento list[], int *tam, evento e) {

    agregarTareaIndice(list, e, *tam); // Agrega la lista al final de la lista

    (*tam)++;

    // Insercion Sort
    ordenar(list, *tam);

}

// Imprime la tarea
void imprimeTarea(const evento e) {

    char h1 = { (e.hr / 10) + '0' }; // Obtiene el primer dígito y lo convierte a char (si es que lo hay, de lo contrario 0)
    char h2 = { (e.hr - (e.hr / 10) * 10) + '0' }; // Obtiene el segundo digito y lo convierte

    char m1 = { (e.minuto / 10) + '0' };
    char m2 = { (e.minuto - (e.minuto / 10) * 10) + '0' };

    printf("%c%c:%c%c - %s", h1, h2, m1, m2, e.descripcion);

}

//Imprime todas las tareas
void imprimeLista(const evento list[], const int tam) {

    if (tam == 0) {
        puts("\n| No hay tareas!\n");
        return;
    }

    char *seperador = "+--------------------------------+";

    printf("\n%s\n|          Scheduler           |\n%s\n\n", seperador, seperador);

    for (int i = 0; i < tam; i++) {
        printf("| [%d] ", i);
        imprimeTarea(list[i]);
    }

    putchar('\n');

}
//Despliegue de los procesos
void iniciarProcesos(){

}
/*void delay(int sec){
    for(int i=(time(NULL)+sec);time(NULL)!=i;time(NULL));
}*/

void *ejecutarLista(const evento list[], const int tam){
    if (tam > 0) {
        
        char *seperador = "+--------------------------------+";

            printf("\n%s\n|          Scheduler           |\n%s\n\n", seperador, seperador);
        for (int i = 0; i < tam; i++) {
            printf("| [%d] ", i);
            imprimeTarea(list[i]);
            printf("la tarea [%d] se esta ejecutando\n",i);
        
        }

    }else{
        puts("\n| No hay tareas!\n");
    }
    
    putchar('\n');

}

/*void ejecutarLista(const evento list[], const int tam){
    
    if (tam == 0) {
        puts("\n| No hay tareas!\n");
        return;
    }

    char *seperador = "+--------------------------------+";

    printf("\n%s\n|          Scheduler           |\n%s\n\n", seperador, seperador);


    for (int i = 0; i < tam; i++) {
        int aleat=rand()%10;
        printf("| [%d] ", i);
        imprimeTarea(list[i]);
        printf("la tarea [%d] se esta ejecutando\n",i);
        delay(aleat);

        if(tam<2){
         printf("La tarea [%d] se ha terminado\n",i);
            break;
        }else if(i%2==0){
            printf("La tarea [%d] se ha terminado\n",i);
            break;
        }else if(i%2==1){
            printf("La tarea [%d] se ha terminado\n",i);
            break;
        }

    }

    putchar('\n');

}*/


int main() {

    evento list[_MAX_TAREAS];
    int i = 0;
    int opc = 0;
    char temp[21];
    

    while (opc != 5) {

        ImprimeMenu(); // Imprime el menu

        printf("%s", "| Seleccione una opcion "); // Prompt for input
        fgets(temp, 21, stdin);
        opc = atoi(temp); // Convierte la opcion a int

        switch (opc) {

        case 1: // Agregar tareas
            if (i + 1 > _MAX_TAREAS) {
                printf("| SOLO PUEDE TENER %d TAREAS ACTIVAS!\n\n", i);
                break;
            }
            ordenaInserta(list, &i, *nuevaTarea(&list[i]));
            system("pause");
            system("cls");
            break;
        case 2: // Elimina tareas
            printf("Todavia no sirve :( \n");
            system("pause");
            system("cls");
            break;
        case 3: // Imprime todos las tareas
            imprimeLista(list, i);
            system("pause");
            system("cls");

            break;
        case 4:
            //ejecutarLista(list,i);
            pthread_t hilo1;
            pthread_create(&hilo1, NULL, ejecutarLista(list,i),NULL);
            pthread_join(hilo1,NULL);
            //system("pause");
            //system("cls");
            break;
        case 5:
            puts("\n| Adios!\n");
            system("pause");
            exit(0);
            break;
        default: // Error
            puts("\n| Error en la seleccion\n");
            break;

        }
    }
}
